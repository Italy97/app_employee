import React, { createContext,useReducer } from 'react';
import { View,StyleSheet } from 'react-native';
import Home from './Screens/Home';
import CreateEmployee from './Screens/CreateEmployee';
import Profile from './Screens/Profile';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {Provider} from 'react-redux'
import {reducer,initState} from './reducers/reducer'

export const MyContext = createContext()
const Stack = createStackNavigator();
const myOptions = {
  title:"My Home",
  headerTintColor:"#333",
  headerStyle:{
    backgroundColor:"white"
  }
}
function App(){
    return (
      <View style={styles.container}>
        <Stack.Navigator>
          <Stack.Screen 
            name="Home" 
            component={Home} 
            options={myOptions}
          />
          <Stack.Screen 
            name="Create" 
            component={CreateEmployee} 
            options={{...myOptions,title:"CreateEmployee"}}
          />
          <Stack.Screen 
            name="Profile" 
            component={Profile}
            options={{...myOptions,title:"Profile"}}
          />
        </Stack.Navigator>
      </View>
    );
  }
  export default () =>{
    const [ state,dispatch] = useReducer(reducer,initState)
    return(
      <MyContext.Provider value ={
        {state,dispatch}
      }>
        <NavigationContainer>
          <App/>
        </NavigationContainer>
      </MyContext.Provider>  
    )
  }
const styles = StyleSheet.create({
  container:{
      flex:1,
      backgroundColor:'#ddd'
  }
})
